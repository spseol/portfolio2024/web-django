from django.contrib import admin

from jokes.models import Joke, TypeJoke

# Register your models here.
admin.site.register(TypeJoke)
admin.site.register(Joke)