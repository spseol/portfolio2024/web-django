# Generated by Django 5.0.6 on 2024-05-28 09:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Joke',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('setup', models.CharField(max_length=255)),
                ('punchline', models.CharField(max_length=255)),
                ('ext_id', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='TypeJoke',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
    ]
