from time import sleep

import requests
from django.core.management.base import BaseCommand

from jokes.models import Joke, TypeJoke


class Command(BaseCommand):
    help = 'Syncs jokes from external API'

    def add_arguments(self, parser):
        parser.add_argument('count', type=int)

    def handle(self, *args, **options):
        count = options.get('count', 1)
        print('Syncing jokes...')
        for i in range(count):
            response = requests.get('https://official-joke-api.appspot.com/random_joke')
            joke_api = response.json()
            joke_type, _ = TypeJoke.objects.get_or_create(name=joke_api.get('type', 'general'))
            joke, created = Joke.objects.update_or_create(ext_id=joke_api.get('id'),
                                                    setup=joke_api.get('setup'),
                                                    punchline=joke_api.get('punchline'),
                                                    type_joke=joke_type)
            if (created):
                print(f'Joke {joke} created')
            else:
                print(f'Joke {joke} updated')
        print('Jokes synced!')
