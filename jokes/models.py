from django.db import models




class TypeJoke(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Joke(models.Model):
    id = models.AutoField(primary_key=True)
    setup = models.CharField(max_length=255)
    punchline = models.CharField(max_length=255)
    # ID from external API
    ext_id = models.IntegerField()

    type_joke = models.ForeignKey(TypeJoke, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.setup} - {self.punchline}"