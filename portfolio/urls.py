from django.urls import path

from portfolio.views import DetailProjectView, ContactMeView

urlpatterns = [
    path('detail/<int:projekt_id>', DetailProjectView.as_view(), name='detail_project'),
    path('add-message', ContactMeView.as_view(), name='add_message'),
]