

from django.contrib import admin
from markdownx.admin import MarkdownxModelAdmin

from portfolio.models import Project, TypeProject, TypeUrl, TypeTag, ProjectFiles, ProjectImages, ContactMe

admin.site.register(TypeProject)
admin.site.register(TypeUrl)
admin.site.register(TypeTag)


class ProjectFilesInline(admin.TabularInline):
    model = ProjectFiles
    extra = 5

class ProjectImagesInline(admin.StackedInline):
    model = ProjectImages
    extra = 5

@admin.register(Project)
class ProjectAdmin(MarkdownxModelAdmin):
    list_display = ('name', 'description', 'logo')
    search_fields = ('name', 'description')
    list_filter = ('name', 'description')
    list_per_page = 10
    readonly_fields = ('created_at', 'modified_at')
    inlines = [ProjectFilesInline, ProjectImagesInline]


admin.site.register(ContactMe)