



from django import forms
from django.core.validators import MinLengthValidator
from django.forms import ModelForm

from portfolio.models import ContactMe


class ContactMeForm(ModelForm):

    text = forms.CharField(widget=forms.Textarea(attrs={'rows': 5}), label='Text', help_text='Zadejte text',
                           validators=[MinLengthValidator(10)])

    class Meta:
        model = ContactMe
        fields = ['email', 'text']
        widgets = {
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }
    labels = {
            'email': 'Email',
            'text': 'Text',
        }
    help_texts = {
            'email': 'Zadejte emailovou adresu',
            'text': 'Zadejte text',
        }

