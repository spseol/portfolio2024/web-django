


from django.http import Http404
from django.shortcuts import render
from django.urls import reverse
from django.views.generic import TemplateView, FormView, CreateView

from portfolio.forms import ContactMeForm
from portfolio.models import Project


# Create your views here.

class DetailProjectView(TemplateView):
    template_name = 'project_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        project_id = kwargs.get('projekt_id', None)
        try:
            context['project'] = Project.objects.get(id=project_id)
        except Project.DoesNotExist:
            context['project'] = None
        context['contact_me'] = ContactMeForm()
        return context

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)


class ContactMeView(CreateView):
    form_class = ContactMeForm
    template_name = 'project_detail.html'
    success_url = '/'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)