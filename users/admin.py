from django.contrib import admin

# Register your models here.

from users.models import Editor

admin.site.register(Editor)
