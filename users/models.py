from django.db import models




class Editor(models.Model):
    user = models.OneToOneField('auth.User',
                                on_delete=models.CASCADE,
                                related_name='editor')

    class Meta:
        verbose_name = 'Editor'
        verbose_name_plural = 'Editoři'

    def __str__(self):
        return self.user.username